# Análisis y predcción (exactitud 90%) de datos sobre infartos
Se realiza un analisis exploratorio de datos (EDA) sobre datos de infartos además uso de algoritmos de Machine Learning para elaborar clasificadores para la predicción de infartos. Algoritmos de clasificadores para Machine Learning que se utilizan son:
- K Nearest Neighbor
- Naive Bayes
- Suport Vector Machine

<h2>Datos</h2>
Los datos utilizados para este análisis se obtuvieron de Kaggle de:  

- https://www.kaggle.com/datasets/rashikrahmanpritom/heart-attack-analysis-prediction-dataset

<br>
<b>Author:</b> Martín López Luna
<br>
<b>LinkedIn:</b> https://www.linkedin.com/in/mart%C3%ADn-l%C3%B3pez-luna-60b947223/
